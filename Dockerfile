FROM openjdk:8
MAINTAINER com.services.micro
ADD conf /conf
ADD server /server
ADD entrypoint.sh /entrypoint.sh
ENV PATH /:$PATH
ENTRYPOINT ["entrypoint.sh"]
